package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAuthRepository {

    void setUserId(@NotNull String userid);

    @Nullable
    String getUserId();

}
