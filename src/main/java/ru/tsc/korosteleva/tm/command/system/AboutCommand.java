package ru.tsc.korosteleva.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Anastasia Korosteleva");
        System.out.println("E-mail: nnkorosteleva@gmail.com");
    }

}
